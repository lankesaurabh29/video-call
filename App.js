import React, { Component } from 'react';

import { Root } from 'native-base';
import StackRoutes from './src/navigation/StackRoutes';


export default class App extends Component {
  componentDidMount() {

  }
  render() {
    return (
      <Root>
        <StackRoutes />
      </Root>
    );
  }
};
