import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from '../screen/SplashScreen';
import DrawerNavigation from './DrawerNavigation';

const Stack = createStackNavigator();
const StackRoutes = () => {

	return (
		<NavigationContainer>
			<Stack.Navigator initialRouteName={SplashScreen} headerMode="none" >
				<Stack.Screen name="SplashScreen" component={SplashScreen} />
				<Stack.Screen name="DrawerNavigation" component={DrawerNavigation} />
			</Stack.Navigator>
		</NavigationContainer>
	);
};


export default StackRoutes;