import React from "react";
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerComponent from '../screen/DrawerScreen';
import HomeScreens from "../screen/HomeScreens";
import VideoConferance from "../screen/VideoConferance";


const DrawerSide = createDrawerNavigator();

export default class DrawerNavigation extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }

  }
  componentDidMount = () => {
  }
  render() {
    return (
      <DrawerSide.Navigator initialRouteName="HomeScreens" drawerContent={props => <DrawerComponent {...props} />}>
        <DrawerSide.Screen name="HomeScreens" component={HomeScreens} options={{ headerShown: true }} />
        <DrawerSide.Screen name="VideoConferance" component={VideoConferance} options={{ headerShown: true }} />
      </DrawerSide.Navigator>
    )
  }
}