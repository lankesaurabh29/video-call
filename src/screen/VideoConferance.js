/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  Button,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {dimensionsScale} from 'react-native-utils-scale';
import WebrtcSimple from 'react-native-webrtc-simple';
import {
  globalCall,
  globalCallRef,
  GlobalCallUI,
} from 'react-native-webrtc-simple/UIKit';

const VideoConference = (props) => {
  const [userId, setUserId] = useState(null);
  const [callId, setCallId] = useState('');

  useEffect(() => {
    const configuration = {
      optional: null,
      key: Math.random().toString(36).substr(2, 4),
    };

    globalCall.start(configuration, (sessionId) => {
      setUserId(sessionId);
    });
    WebrtcSimple.listenings.callEvents((type, userData) => {   
        console.log('Type: ', type);

        // START_CALL
        // RECEIVED_CALL
        // REJECT_CALL
        // ACCEPT_CALL
        // END_CALL   
      });
  
      WebrtcSimple.listenings.getRemoteStream((remoteStream) => {
        console.log('Remote stream', remoteStream);
      });
  }, []);

  const callToUser = (userId) => {
    if (userId.length > 0) {
      const data = {
        sender_name: 'Sender Name',
        sender_avatar:
          'https://www.atlantawatershed.org/wp-content/uploads/2017/06/default-placeholder.png',
        receiver_name: 'Receiver Name',
        receiver_avatar:
          'https://www.atlantawatershed.org/wp-content/uploads/2017/06/default-placeholder.png',
      };
      WebrtcSimple.events.call(userId, data);
    } else {
      alert('Please enter userId');
    }
  };

  return (
    <View style={styles.container}>
      <View>
        <Text style={{fontSize: 30}}>{userId}</Text>
      </View>

      <View style={styles.rowbtn}>
        <TextInput
          style={styles.textInput}
          autoCapitalize="none"
          keyboardType="default"
          placeholder="Enter id"
          onChangeText={(text) => {
            setCallId(text);
          }}
        />
        <View style={styles.btn}>
          <Button
            title="Call"
            color={Platform.OS === 'ios' ? 'white' : 'black'}
            onPress={() => {
              callToUser(callId);
            }}
          />
        </View>
      </View>
      <GlobalCallUI ref={globalCallRef} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowbtn: {
    flexDirection: 'row',
    paddingHorizontal: 16 * dimensionsScale.scale(),
    alignItems: 'center',
    marginVertical: 8 * dimensionsScale.scale(),
  },
  btn: {
    margin: 16 * dimensionsScale.scale(),
    backgroundColor: 'black',
    paddingHorizontal: 10 * dimensionsScale.scale(),
  },
  textInput: {
    width: 200 * dimensionsScale.scale(),
    height: 50 * dimensionsScale.scale(),
    borderWidth: 0.5 * dimensionsScale.scale(),
    borderColor: 'gray',
    paddingHorizontal: 12 * dimensionsScale.scale(),
  },
});

export default VideoConference;