import React from "react";
import { View, Text, Image, Alert, Linking, TouchableOpacity, } from "react-native";
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';

import { Icon } from 'native-base'



const CustomeDrawerItem = (props) => (
    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", borderWidth: 0 }} onPress={props.onPressItem}>
        {/* <View style={{ width: 30, justifyContent: "center", alignItems: "center" }}>
            <Icon type="FontAwesome" name={props.iconName} />
        </View> */}
        <Text style={{ fontWeight: 'bold', fontSize: 15 }}>{props.itemName}</Text>
    </TouchableOpacity>
)

export default class DrawerScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userInfo: null,
        }

    }

    onPressDrawerItem = (screen) => {
        this.props.navigation.navigate(screen)
    }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "white", }}>

                <View style={{ height: "20%", borderWidth: 0, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ borderWidth: 1, height: '70%', width: '35%', borderRadius: 50, alignItems: 'center', justifyContent: 'center' }}>
                        <Icon type="FontAwesome" name="user-circle" />
                    </View>
                </View>
                <DrawerContentScrollView {...this.props}>
                    <View style={{ paddingHorizontal: 20, borderWidth: 0 }}>
                        <CustomeDrawerItem itemName="Home" iconName="home" onPressItem={this.onPressDrawerItem.bind(this, 'HomeScreens')} />
                        <CustomeDrawerItem itemName="Video Conferance" iconName="home" onPressItem={this.onPressDrawerItem.bind(this, 'VideoConferance')} />
                    </View>
                </DrawerContentScrollView>

            </View>
        )
    }
}